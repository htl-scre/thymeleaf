package at.htlstp.thymeleaf.service;

import at.htlstp.thymeleaf.domain.Pizza;
import at.htlstp.thymeleaf.persistence.PizzaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public record PizzaService(PizzaRepository pizzaRepository) {

    public List<Pizza> findAllIncludingIngredients() {
        return pizzaRepository.findAllIncludingIngredients();
    }

    public Pizza findById(long id) {
        return pizzaRepository.findById(id)
                .orElseThrow();
        // Exception -> Handler -> 404, message
        // return ResponseEntity(400)
        // throw new ResponseStatusException(400, "not ready");
    }

    public Pizza save(Pizza pizza) {
        return pizzaRepository.save(pizza);
    }
}
