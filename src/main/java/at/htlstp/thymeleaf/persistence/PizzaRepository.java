package at.htlstp.thymeleaf.persistence;

import at.htlstp.thymeleaf.domain.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PizzaRepository extends JpaRepository<Pizza, Long> {

    @Query("""
            select pizza from Pizza pizza
            join fetch pizza.ingredients
                        """)
    List<Pizza> findAllIncludingIngredients();
}