package at.htlstp.thymeleaf.persistence;

import at.htlstp.thymeleaf.domain.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

}
