package at.htlstp.thymeleaf.presentation;

import at.htlstp.thymeleaf.persistence.IngredientRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public record IngredientController(IngredientRepository ingredientRepository) {

    @GetMapping("ingredients")
    public ModelAndView getAll() {
        var model = Map.of("ingredients", ingredientRepository.findAll());
        var viewName = "ingredient-list";
        return new ModelAndView(viewName, model);
    }
}