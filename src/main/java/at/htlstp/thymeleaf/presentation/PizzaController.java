package at.htlstp.thymeleaf.presentation;


import at.htlstp.thymeleaf.domain.Pizza;
import at.htlstp.thymeleaf.persistence.IngredientRepository;
import at.htlstp.thymeleaf.service.PizzaService;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("pizzas")
public record PizzaController(PizzaService pizzaService,
                              IngredientRepository ingredientRepository) {

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("pizzas", pizzaService.findAllIncludingIngredients());
        return "pizza-list";
    }

    @GetMapping("{id}")
    public String getOne(Model model, @PathVariable long id) {

        model.addAttribute("pizza", pizzaService.findById(id));
        return "pizza-details";
    }

    @GetMapping("new")
    public String getPizzaForm(Model model) {
        var ingredients = ingredientRepository.findAll();
        model.addAttribute("ingredients", ingredients);
        if (!model.containsAttribute("pizza")) {
            var blankPizza = new Pizza(null, "New pizza", "A new pizza", ingredients.subList(0, 2));
            model.addAttribute("pizza", blankPizza);
        }
        return "pizza-edit";
    }

    @PostMapping("new")
    public String addNewPizza(@Valid Pizza pizza,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors()) {
            return getPizzaForm(model);
        }
        //pizza.setDescription("A new pizza"); // doesnt work from getForm-method
        pizzaService.save(pizza);
        return "redirect:/pizzas";
    }
}
